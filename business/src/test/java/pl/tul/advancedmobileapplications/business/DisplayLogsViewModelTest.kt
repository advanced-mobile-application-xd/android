package pl.tul.advancedmobileapplications.business

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.ArgumentCaptor
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import pl.tul.advancedmobileapplications.business.database.room.dao.LogEntryDao
import pl.tul.advancedmobileapplications.business.logger.Logger
import pl.tul.advancedmobileapplications.business.logger.PersistentLogger
import pl.tul.advancedmobileapplications.business.viewmodel.DisplayLogsViewModel
import pl.tul.advancedmobileapplications.model.logger.LogEntry


/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(JUnit4::class)
class DisplayLogsViewModelTest {
    @Rule @JvmField  var rule: TestRule = InstantTaskExecutorRule()

    @Mock
    lateinit var logger: PersistentLogger

    @Mock
    lateinit var logEntryDao: LogEntryDao

    lateinit var displayLogsViewModel: DisplayLogsViewModel

    private val observer: Observer<List<LogEntry>> = mock()

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)

        Mockito.`when`(logger.getLogs()).thenAnswer {

            val mutableLiveData = MutableLiveData<List<LogEntry>>()

            val logEntryList = ArrayList<LogEntry>()
            mutableLiveData.postValue(logEntryList)

            return@thenAnswer mutableLiveData
        }

    }

    @Test
    fun checkDisplayLogsViewModelSetup() {
        this.displayLogsViewModel = DisplayLogsViewModel(logger)
        this.displayLogsViewModel.logs.observeForever(observer)

        val captor = ArgumentCaptor.forClass(DisplayLogsViewModel::class.java)
        captor.run {
            verify(logger, times(1)).getLogs()
        }
    }


    @Test
    fun checkDisplayLogsViewModelLogClearing() {
        this.displayLogsViewModel = DisplayLogsViewModel(logger)
        this.displayLogsViewModel.logs.observeForever(observer)
        this.displayLogsViewModel.clearLogs()

        val captor = ArgumentCaptor.forClass(DisplayLogsViewModel::class.java)
        captor.run {
            verify(logger, times(1)).clearLogs()
        }
    }

    @Test
    fun checkDisplayLogsViewModelLogTesting() {
        this.displayLogsViewModel = DisplayLogsViewModel(logger)
        this.displayLogsViewModel.logs.observeForever(observer)
        this.displayLogsViewModel.testLog("test")

        val captor = ArgumentCaptor.forClass(DisplayLogsViewModel::class.java)
        captor.run {
            verify(logger, times(1)).log("test")
        }
    }

    //...
}