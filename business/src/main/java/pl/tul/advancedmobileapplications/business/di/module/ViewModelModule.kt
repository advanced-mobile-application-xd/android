package pl.tul.advancedmobileapplications.business.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import pl.tul.advancedmobileapplications.business.di.factory.viewmodel.ViewModelFactory
import pl.tul.advancedmobileapplications.business.di.factory.viewmodel.ViewModelKey
import pl.tul.advancedmobileapplications.business.viewmodel.DisplayLogsViewModel
import pl.tul.advancedmobileapplications.business.viewmodel.LocationTrackingViewModel

@Module
interface ViewModelModule {

    @Binds
    fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(DisplayLogsViewModel::class)
    fun bindDisplayLogsViewModel(viewModel: DisplayLogsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(LocationTrackingViewModel::class)
    fun bindLocationTrackingViewModel(viewModel: LocationTrackingViewModel): ViewModel
}
