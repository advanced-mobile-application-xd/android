package pl.tul.advancedmobileapplications.business.viewmodel

import androidx.lifecycle.ViewModel
import pl.tul.advancedmobileapplications.business.logger.PersistentLogger
import javax.inject.Inject

class DisplayLogsViewModel @Inject constructor(
    private val logger: PersistentLogger
) : ViewModel() {

    val logs = logger.getLogs()

    fun testLog(message: String) {
        logger.log(message)
    }

    fun clearLogs() {
        logger.clearLogs()
    }
}