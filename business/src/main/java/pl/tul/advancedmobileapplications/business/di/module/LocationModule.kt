package pl.tul.advancedmobileapplications.business.di.module

import android.content.Context
import com.google.android.gms.location.ActivityRecognitionClient
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import dagger.Module
import dagger.Provides
import pl.tul.advancedmobileapplications.business.di.scope.BusinessLogicScope

@Module
class LocationModule {

    @BusinessLogicScope
    @Provides
    fun provideFusedLocationProviderClient(
        context: Context
    ): FusedLocationProviderClient =
        LocationServices.getFusedLocationProviderClient(context)

    @Provides
    fun provideActivityRecognitionClient(
        context: Context
    ): ActivityRecognitionClient =
        ActivityRecognitionClient(context)
}