package pl.tul.advancedmobileapplications.business.database.room

import androidx.room.Database
import androidx.room.RoomDatabase
import pl.tul.advancedmobileapplications.business.database.room.dao.LogEntryDao
import pl.tul.advancedmobileapplications.model.logger.LogEntry

@Database(
    entities = [
        LogEntry::class
    ],
    version = GeoFenceDatabase.DATABASE_VERSION,
    exportSchema = false
)
abstract class GeoFenceDatabase : RoomDatabase() {

    abstract fun logEntryDao(): LogEntryDao

    companion object {
        const val DATABASE_VERSION = 2
    }
}