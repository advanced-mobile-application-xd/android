package pl.tul.advancedmobileapplications.business.network.manager

import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import pl.tul.advancedmobileapplications.business.logger.SingleConnectionLogger
import pl.tul.advancedmobileapplications.business.network.interceptor.AuthorizationInterceptor
import pl.tul.advancedmobileapplications.business.network.service.ApiService
import pl.tul.advancedmobileapplications.model.api.ConfigurationResponse
import pl.tul.advancedmobileapplications.model.api.PostReportRequest
import pl.tul.advancedmobileapplications.model.resource.Resource
import javax.inject.Inject

class ApiManager @Inject constructor(
    private val apiService: ApiService,
    private val singleConnectionLogger: SingleConnectionLogger,
    private val connectivityManager: ConnectivityManager?,
    private val authorizationInterceptor: AuthorizationInterceptor
) {

    private val _configuration = MutableLiveData<Resource<ConfigurationResponse>>()
    val configuration: LiveData<Resource<ConfigurationResponse>>
        get() = _configuration

    // TODO: Cleanup logging
    fun getConfiguration(id: Int) {
        _configuration.value = Resource.loading()
        GlobalScope.launch(Dispatchers.IO) {
            try {
                val response = apiService.getConfiguration(id)
                authorizationInterceptor.setAuthToken(response.token)
                val parsed = Gson().toJson(response)
                singleConnectionLogger.log("getConfiguration :: $parsed")
                _configuration.postValue(Resource.success(response))
            } catch (exception: Exception) {
                if (isNetworkAvailable()) {
                    singleConnectionLogger.log("getConfiguration :: fail - ${exception.localizedMessage}")
                    _configuration.postValue(Resource.error("Error"))
                } else {
                    singleConnectionLogger.log("getConfiguration :: no internet")
                    _configuration.postValue(Resource.noConnection())
                }
            }
        }
    }

    fun postReport(reportData: PostReportRequest) {
        val parsed = Gson().toJson(reportData)
        singleConnectionLogger.log("postReport :: $parsed")
        GlobalScope.launch(Dispatchers.IO) {
            try {
                apiService.postReport(reportData)
                singleConnectionLogger.log("postReport :: success")
            } catch (exception: Exception) {
                if (isNetworkAvailable()) {
                    singleConnectionLogger.log("postReport :: fail - ${exception.localizedMessage}")
                } else {
                    singleConnectionLogger.log("postReport :: no internet")
                }
            }
        }
    }

    fun clearAuthData() {
        authorizationInterceptor.removeAuthToken()
    }

    private fun isNetworkAvailable(): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val nw = connectivityManager?.activeNetwork ?: return false
            val actNw = connectivityManager.getNetworkCapabilities(nw) ?: return false
            return when {
                actNw.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                actNw.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                actNw.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
                else -> false
            }
        } else {
            val nwInfo = connectivityManager?.activeNetworkInfo ?: return false
            return nwInfo.isConnected
        }
    }
}