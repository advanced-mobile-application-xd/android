package pl.tul.advancedmobileapplications.business.location

import android.Manifest
import android.app.PendingIntent
import android.content.Context
import android.content.pm.PackageManager
import android.os.BatteryManager
import android.os.Build
import androidx.core.content.ContextCompat
import com.google.android.gms.location.*
import pl.tul.advancedmobileapplications.business.R
import pl.tul.advancedmobileapplications.business.logger.SingleConnectionLogger
import pl.tul.advancedmobileapplications.business.network.manager.ApiManager
import pl.tul.advancedmobileapplications.model.api.LocationData
import pl.tul.advancedmobileapplications.model.api.PostReportRequest
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class LocationTracker @Inject constructor(
    private val singleConnectionLogger: SingleConnectionLogger,
    private val apiManager: ApiManager,
    private val activityRecognitionClient: ActivityRecognitionClient,
    private val locationClient: FusedLocationProviderClient,
    private val batteryManager: BatteryManager?,
    private val context: Context
) {

    private lateinit var activityRecognitionPendingIntent: PendingIntent
    private lateinit var locationRequest: LocationRequest

    private var isRegisteredForUpdate = false
    private var trackedObjectId = ""

    private val locationCallback: LocationCallback by lazy {
        object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                locationResult ?: return
                with(locationResult.lastLocation) {
                    apiManager.postReport(
                        PostReportRequest(
                            location = LocationData(
                                acc = accuracy.toDouble(),
                                alt = altitude,
                                bea = bearing.toDouble(),
                                lat = latitude,
                                long = longitude,
                                prov = provider,
                                spd = speed.toDouble(),
                                sat = 0,
                                time = getCurrentTimestamp(),
                                serial = "",
                                tid = trackedObjectId,
                                plat = LocationData.PLATFORM_ANDROID,
                                platVer = Build.VERSION.RELEASE,
                                bat = batteryManager?.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY)?.toDouble()
                            )
                        )
                    )
                }
            }
        }
    }

    fun startLogging(
        trackedObjectId: String,
        intervalInMinutes: Int,
        activityRecognitionPendingIntent: PendingIntent
    ) {
        singleConnectionLogger.log(context.getString(R.string.log_location_tracking_started))

        this.trackedObjectId = trackedObjectId
        this.locationRequest = LocationRequest.create()
            .setInterval(intervalInMinutes * 60 * 1000L)
            .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
        this.activityRecognitionPendingIntent = activityRecognitionPendingIntent

        if (hasPermission(Manifest.permission.ACCESS_FINE_LOCATION)) {
            isRegisteredForUpdate = true
            locationClient.requestLocationUpdates(locationRequest, locationCallback, null)
        }

        activityRecognitionClient.requestActivityUpdates(
            ACTIVITY_RECOGNITION_API_INTERVAL_MS,
            activityRecognitionPendingIntent
        )
    }

    fun stopLogging() {
        singleConnectionLogger.log(context.getString(R.string.log_location_tracking_stopped))
        singleConnectionLogger.clearLogs()
        activityRecognitionClient.removeActivityUpdates(activityRecognitionPendingIntent)
        locationClient.removeLocationUpdates(locationCallback)
        apiManager.clearAuthData()
        isRegisteredForUpdate = false
    }

    fun updateDetectedActivity(detectedActivity: DetectedActivity) {
        singleConnectionLogger.log("ActivityRecognitionAPI: $detectedActivity")
        if (detectedActivity.type == DetectedActivity.STILL && isRegisteredForUpdate) {
            locationClient.removeLocationUpdates(locationCallback)
            isRegisteredForUpdate = false
            singleConnectionLogger.log(context.getString(R.string.log_location_tracking_paused))
        } else if (detectedActivity.type != DetectedActivity.STILL && !isRegisteredForUpdate) {
            if (hasPermission(Manifest.permission.ACCESS_FINE_LOCATION)) {
                isRegisteredForUpdate = true
                locationClient.requestLocationUpdates(locationRequest, locationCallback, null)
                singleConnectionLogger.log(context.getString(R.string.log_location_tracking_resumed))
            }
        }
    }

    private fun hasPermission(permission: String): Boolean =
        ContextCompat.checkSelfPermission(context, permission) == PackageManager.PERMISSION_GRANTED

    companion object {

        const val ACTIVITY_RECOGNITION_API_INTERVAL_MS = 5000L

        private const val SDF_PATTERN = "yyyy-MM-dd'T'HH:mm:ss'Z'"

        private fun getCurrentTimestamp(): String {
            val now = Calendar.getInstance()
            return SimpleDateFormat(SDF_PATTERN, Locale.getDefault()).format(now.time)
        }
    }
}