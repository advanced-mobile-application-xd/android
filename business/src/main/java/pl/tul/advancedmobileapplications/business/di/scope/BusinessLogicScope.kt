package pl.tul.advancedmobileapplications.business.di.scope

import javax.inject.Scope

@Scope
@Retention
annotation class BusinessLogicScope