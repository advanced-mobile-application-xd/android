package pl.tul.advancedmobileapplications.business.permission

import android.app.Activity
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.single.BasePermissionListener
import pl.tul.advancedmobileapplications.business.R
import pl.tul.advancedmobileapplications.business.logger.PersistentLogger
import javax.inject.Inject

class PermissionManager @Inject constructor(
    private val logger: PersistentLogger
) {

    fun askForPermission(
        activity: Activity,
        permission: String,
        onDenied: (permanentlyDenied: Boolean) -> Unit,
        onSuccess: () -> Unit
    ) {
        Dexter.withActivity(activity)
            .withPermission(permission)
            .withListener(object : BasePermissionListener() {

                private var rationaleShouldBeShown = false

                override fun onPermissionGranted(response: PermissionGrantedResponse?) {
                    logger.log(
                        activity.getString(
                            R.string.log_permission_granted,
                            response?.permissionName
                        )
                    )
                    onSuccess()
                }

                override fun onPermissionRationaleShouldBeShown(
                    permission: PermissionRequest?,
                    token: PermissionToken?
                ) {
                    rationaleShouldBeShown = true
                    token?.continuePermissionRequest()
                }

                override fun onPermissionDenied(response: PermissionDeniedResponse?) {
                    val permissionName = response?.permissionName
                    if (response?.isPermanentlyDenied == true) {
                        logger.log(
                            activity.getString(
                                R.string.log_permission_permanently_denied,
                                permissionName
                            )
                        )
                        onDenied(true)
                    } else {
                        logger.log(
                            activity.getString(
                                R.string.log_permission_denied,
                                permissionName
                            )
                        )
                        onDenied(false)
                    }
                }
            })
            .check()
    }
}