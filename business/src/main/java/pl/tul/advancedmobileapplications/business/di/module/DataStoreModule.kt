package pl.tul.advancedmobileapplications.business.di.module

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import pl.tul.advancedmobileapplications.business.database.room.GeoFenceDatabase
import pl.tul.advancedmobileapplications.business.database.room.dao.LogEntryDao
import pl.tul.advancedmobileapplications.business.di.scope.BusinessLogicScope

@Module
class DataStoreModule {

    @Provides
    @BusinessLogicScope
    fun provideGeoFenceDatabase(
        context: Context
    ): GeoFenceDatabase = Room.databaseBuilder(
        context,
        GeoFenceDatabase::class.java,
        GEO_FENCE_DATABASE_NAME
    )
        .fallbackToDestructiveMigration()
        .build()


    @Provides
    @BusinessLogicScope
    fun provideLogEntryDao(
        geoFenceDatabase: GeoFenceDatabase
    ): LogEntryDao = geoFenceDatabase.logEntryDao()

    companion object {
        const val GEO_FENCE_DATABASE_NAME = "geoFenceDb"
    }
}