package pl.tul.advancedmobileapplications.business.logger

import androidx.lifecycle.LiveData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import pl.tul.advancedmobileapplications.business.database.room.dao.LogEntryDao
import pl.tul.advancedmobileapplications.business.di.scope.BusinessLogicScope
import pl.tul.advancedmobileapplications.model.logger.LogEntry
import javax.inject.Inject

@BusinessLogicScope
class PersistentLogger @Inject constructor(
    private val logEntryDao: LogEntryDao
) : Logger {

    override fun log(message: String) {
        val entry = LogEntry(System.currentTimeMillis(), message, Logger.LOG_TYPE_PERSISTENT)
        GlobalScope.launch(Dispatchers.IO) {
            logEntryDao.add(entry)
        }
    }

    override fun clearLogs() {
        GlobalScope.launch(Dispatchers.IO) {
            logEntryDao.clear(Logger.LOG_TYPE_PERSISTENT)
        }
    }

    override fun getLogs(): LiveData<List<LogEntry>> =
        logEntryDao.getType(Logger.LOG_TYPE_PERSISTENT)
}