package pl.tul.advancedmobileapplications.business.network.service

import pl.tul.advancedmobileapplications.model.api.ConfigurationResponse
import pl.tul.advancedmobileapplications.model.api.PostReportRequest
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface ApiService {

    @GET("configurations/{id}")
    suspend fun getConfiguration(@Path("id") id: Int): ConfigurationResponse

    @POST("reports/")
    suspend fun postReport(@Body reportData: PostReportRequest)
}