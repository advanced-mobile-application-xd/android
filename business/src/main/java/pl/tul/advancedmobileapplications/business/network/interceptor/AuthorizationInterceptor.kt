package pl.tul.advancedmobileapplications.business.network.interceptor

import okhttp3.Interceptor
import okhttp3.Response

class AuthorizationInterceptor : Interceptor {

    private var token: String? = null

    override fun intercept(chain: Interceptor.Chain): Response {
        val requestBuilder = chain.request().newBuilder()
        token?.let { token ->
            requestBuilder.addHeader(AUTH_TOKEN_HEADER, token)
        }
        return chain.proceed(requestBuilder.build())
    }

    fun setAuthToken(token: String) {
        this.token = String.format(AUTH_TOKEN_VALUE_FORMAT, token)
    }

    fun removeAuthToken() {
        this.token = null
    }

    companion object {

        private const val AUTH_TOKEN_HEADER = "Authorization"
        private const val AUTH_TOKEN_VALUE_FORMAT = "Bearer: %s"
    }
}