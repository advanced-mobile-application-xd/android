package pl.tul.advancedmobileapplications.business.di.module

import android.content.Context
import android.net.ConnectivityManager
import android.os.BatteryManager
import androidx.core.content.ContextCompat
import dagger.Module
import dagger.Provides
import pl.tul.advancedmobileapplications.business.di.scope.BusinessLogicScope

@Module
class SystemServiceModule {

    @Provides
    @BusinessLogicScope
    fun provideConnectivityManager(context: Context): ConnectivityManager? =
        ContextCompat.getSystemService(context, ConnectivityManager::class.java)

    @BusinessLogicScope
    @Provides
    fun provideBatteryManager(
        context: Context
    ): BatteryManager? =
        ContextCompat.getSystemService(context, BatteryManager::class.java)

}