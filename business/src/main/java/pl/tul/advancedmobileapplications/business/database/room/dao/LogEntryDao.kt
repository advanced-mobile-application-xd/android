package pl.tul.advancedmobileapplications.business.database.room.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import pl.tul.advancedmobileapplications.model.logger.LogEntry

@Dao
interface LogEntryDao {

    @Query("SELECT * FROM logentry")
    fun getAll(): LiveData<List<LogEntry>>

    @Query("SELECT * FROM logentry WHERE logType = :logType")
    fun getType(logType: Int): LiveData<List<LogEntry>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun add(entry: LogEntry)

    @Query("DELETE FROM logentry WHERE logType = :logType")
    suspend fun clear(logType: Int)
}