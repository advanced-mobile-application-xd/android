package pl.tul.advancedmobileapplications.business.di.module

import dagger.Module
import dagger.Provides
import okhttp3.HttpUrl
import okhttp3.HttpUrl.Companion.toHttpUrl
import okhttp3.OkHttpClient
import pl.tul.advancedmobileapplications.business.di.scope.BusinessLogicScope
import pl.tul.advancedmobileapplications.business.network.interceptor.AuthorizationInterceptor
import pl.tul.advancedmobileapplications.business.network.service.ApiService
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

@Module
class NetworkModule {

    @Provides
    @BusinessLogicScope
    fun provideAuthorizationInterceptor(): AuthorizationInterceptor {
        return AuthorizationInterceptor()
    }

    @Provides
    @BusinessLogicScope
    fun provideOkHttpClient(
        authorizationInterceptor: AuthorizationInterceptor
    ): OkHttpClient {
        return OkHttpClient.Builder()
            .addInterceptor(authorizationInterceptor)
            .build()
    }

    @Provides
    @BusinessLogicScope
    fun provideRetrofitClient(
        client: OkHttpClient,
        converterFactory: Converter.Factory
    ): Retrofit {
        val baseUrl: HttpUrl = API_URL.toHttpUrl()
        return Retrofit.Builder()
            .baseUrl(baseUrl)
            .client(client)
            .addConverterFactory(converterFactory)
            .build()
    }

    @Provides
    @BusinessLogicScope
    fun provideConverterFactory(): Converter.Factory {
        return GsonConverterFactory.create()
    }

    @Provides
    @BusinessLogicScope
    fun provideApiService(
        retrofit: Retrofit
    ): ApiService {
        return retrofit.create(ApiService::class.java)
    }

    companion object {
        const val API_URL = "https://ama-rb.herokuapp.com/"
    }
}
