package pl.tul.advancedmobileapplications.business.di.provider

import pl.tul.advancedmobileapplications.business.di.component.BusinessComponent

interface BusinessComponentProvider {

    fun provideBusinessComponent(): BusinessComponent
}