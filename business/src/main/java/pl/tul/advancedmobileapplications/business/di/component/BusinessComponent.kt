package pl.tul.advancedmobileapplications.business.di.component

import android.content.Context
import androidx.lifecycle.ViewModelProvider
import dagger.BindsInstance
import dagger.Component
import pl.tul.advancedmobileapplications.business.di.module.*
import pl.tul.advancedmobileapplications.business.di.scope.BusinessLogicScope
import pl.tul.advancedmobileapplications.business.location.LocationTracker
import pl.tul.advancedmobileapplications.business.logger.SingleConnectionLogger
import pl.tul.advancedmobileapplications.business.permission.PermissionManager

@Component(
    modules = [
        NetworkModule::class,
        ViewModelModule::class,
        DataStoreModule::class,
        LocationModule::class,
        SystemServiceModule::class
    ]
)
@BusinessLogicScope
interface BusinessComponent {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun context(context: Context): Builder

        fun build(): BusinessComponent
    }

    fun provideViewModelFactory(): ViewModelProvider.Factory

    fun providePermissionManager(): PermissionManager

    fun provideLogger(): SingleConnectionLogger

    fun provideLocationLogger(): LocationTracker
}
