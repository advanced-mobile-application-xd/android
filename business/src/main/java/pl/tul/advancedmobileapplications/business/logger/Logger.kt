package pl.tul.advancedmobileapplications.business.logger

import androidx.lifecycle.LiveData
import pl.tul.advancedmobileapplications.model.logger.LogEntry

interface Logger {

    fun log(message: String)

    fun clearLogs()

    fun getLogs(): LiveData<List<LogEntry>>

    companion object {
        const val LOG_TYPE_PERSISTENT = 0
        const val LOG_TYPE_SINGLE_CONNECTION = 1
    }
}