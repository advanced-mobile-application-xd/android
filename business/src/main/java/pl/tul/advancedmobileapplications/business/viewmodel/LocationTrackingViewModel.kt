package pl.tul.advancedmobileapplications.business.viewmodel

import androidx.lifecycle.ViewModel
import pl.tul.advancedmobileapplications.business.logger.PersistentLogger
import pl.tul.advancedmobileapplications.business.logger.SingleConnectionLogger
import pl.tul.advancedmobileapplications.business.network.manager.ApiManager
import javax.inject.Inject

class LocationTrackingViewModel @Inject constructor(
    private val apiManager: ApiManager,
    private val persistentLogger: PersistentLogger,
    private val singleConnectionLogger: SingleConnectionLogger
) : ViewModel() {

    val configuration = apiManager.configuration
    val logs = singleConnectionLogger.getLogs()

    fun getConfiguration(id: Int) {
        apiManager.getConfiguration(id)
    }

    fun clearLogs() {
        singleConnectionLogger.clearLogs()
    }
}