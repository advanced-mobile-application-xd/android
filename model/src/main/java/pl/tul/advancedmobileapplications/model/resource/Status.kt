package pl.tul.advancedmobileapplications.model.resource

enum class Status {
    LOADING,
    SUCCESS,
    ERROR,
    NO_CONNECTION
}