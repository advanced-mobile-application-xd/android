package pl.tul.advancedmobileapplications.model.resource

import pl.tul.advancedmobileapplications.model.resource.Status.*

data class Resource<out T>(
    val status: Status,
    val data: T? = null,
    val errorMessage: String? = null,
    val errorException: Throwable? = null
) {
    companion object {

        fun <T> success(data: T?): Resource<T> =
            Resource(SUCCESS, data)

        fun <T> error(msg: String?, errorException: Throwable? = null): Resource<T> =
            Resource(ERROR, errorMessage = msg, errorException = errorException)

        fun <T> loading(): Resource<T> =
            Resource(LOADING)

        fun <T> noConnection(): Resource<T> =
            Resource(NO_CONNECTION)

    }
}