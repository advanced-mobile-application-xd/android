package pl.tul.advancedmobileapplications.model.api

data class LocationData(
    val acc: Double,
    val alt: Double,
    val bea: Double,
    val lat: Double,
    val long: Double,
    val prov: String,
    val spd: Double,
    val sat: Int,
    val time: String,
    val serial: String,
    val tid: String,
    val plat: String,
    val platVer: String,
    val bat: Double?
) {

    companion object {

        const val PLATFORM_ANDROID = "Android"
    }
}