package pl.tul.advancedmobileapplications.model.api

data class PostReportRequest(
    val location: LocationData
)