package pl.tul.advancedmobileapplications.model.logger

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class LogEntry(
    val timestamp: Long,
    val message: String,
    val logType: Int
) {
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0
}