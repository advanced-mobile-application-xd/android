package pl.tul.advancedmobileapplications.model.api

data class ConfigurationResponse(
    val name: String,
    val token: String,
    val trackedObjectId: String,
    val positionIntervalInMinutes: Int
)