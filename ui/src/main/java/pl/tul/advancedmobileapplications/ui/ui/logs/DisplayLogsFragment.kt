package pl.tul.advancedmobileapplications.ui.ui.logs

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.fragment_display_logs.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.noButton
import org.jetbrains.anko.yesButton
import pl.tul.advancedmobileapplications.business.viewmodel.DisplayLogsViewModel
import pl.tul.advancedmobileapplications.model.logger.LogEntry
import pl.tul.advancedmobileapplications.ui.R
import pl.tul.advancedmobileapplications.ui.extension.takeIfValid
import pl.tul.advancedmobileapplications.ui.extension.viewModel
import pl.tul.advancedmobileapplications.ui.ui.common.BaseFragment
import pl.tul.advancedmobileapplications.ui.ui.common.adapter.LogsAdapter

class DisplayLogsFragment : BaseFragment(R.layout.fragment_display_logs) {

    private val viewModel: DisplayLogsViewModel by viewModel()
    private val logsAdapter by lazy { LogsAdapter() }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupLogsRecycler()
        setupListeners()
        setupObservers()
    }

    private fun setupLogsRecycler() {
        logsRecycler.apply {
            layoutManager =
                LinearLayoutManager(this@DisplayLogsFragment.context, RecyclerView.VERTICAL, false)
            adapter = logsAdapter
            addItemDecoration(
                DividerItemDecoration(
                    this@DisplayLogsFragment.context,
                    RecyclerView.VERTICAL
                )
            )
        }
    }

    private fun setupListeners() {
        testLogButton.setOnClickListener {
            viewModel.testLog(getString(R.string.label_test_log_message))
        }
        clearLogsButton.setOnClickListener {
            activity?.takeIfValid()?.alert(
                R.string.alert_clear_logs_message,
                R.string.alert_clear_device_logs_title
            ) {
                yesButton {
                    viewModel.clearLogs()
                }
                noButton {
                    // Do nothing
                }
            }?.show()
        }
    }

    private fun setupObservers() {
        viewModel.logs.observe(viewLifecycleOwner, Observer {
            updateLogs(it ?: listOf())
        })
    }

    private fun updateLogs(logs: List<LogEntry>) {
        logsAdapter.logs = logs
    }
}