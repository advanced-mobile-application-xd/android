package pl.tul.advancedmobileapplications.ui.ui.main

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.setupActionBarWithNavController
import kotlinx.android.synthetic.main.activity_main.*
import pl.tul.advancedmobileapplications.ui.R
import pl.tul.advancedmobileapplications.ui.extension.lazyExtra
import pl.tul.advancedmobileapplications.ui.ui.common.BaseActivity
import pl.tul.advancedmobileapplications.ui.ui.configuration.SelectConfigurationFragmentDirections

class MainActivity : BaseActivity() {

    private val configurationId by lazyExtra<Int>(INTENT_EXTRA_CONFIGURATION_ID)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupActionBarWithNavController(navHostFragment.findNavController())
        handleIntentExtras()
    }

    override fun onSupportNavigateUp(): Boolean {
        return navHostFragment.findNavController().navigateUp() || super.onSupportNavigateUp()
    }

    private fun handleIntentExtras() {
        configurationId?.let {
            val action = SelectConfigurationFragmentDirections
                .actionSelectConfigurationFragmentToLocationTrackingFragment(it, false)
            navHostFragment.findNavController().navigate(action)
        }
    }

    companion object {

        private const val INTENT_EXTRA_CONFIGURATION_ID = "CONFIGURATION_ID"

        fun getTrackingDetailsIntent(context: Context, configurationId: Int): Intent {
            val intent = Intent(context, MainActivity::class.java)
            intent.putExtra(INTENT_EXTRA_CONFIGURATION_ID, configurationId)
            return intent
        }
    }
}