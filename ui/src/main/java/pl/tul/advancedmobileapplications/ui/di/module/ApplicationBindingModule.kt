package pl.tul.advancedmobileapplications.ui.di.module

import android.content.Context
import dagger.Binds
import dagger.Module
import pl.tul.advancedmobileapplications.ui.GeoFenceApplication
import pl.tul.advancedmobileapplications.ui.di.scope.UiModuleScope

@Module
interface ApplicationBindingModule {

    @Binds
    @UiModuleScope
    fun bindApplicationContext(application: GeoFenceApplication): Context
}