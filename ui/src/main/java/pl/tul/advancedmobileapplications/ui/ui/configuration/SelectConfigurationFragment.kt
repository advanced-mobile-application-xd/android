package pl.tul.advancedmobileapplications.ui.ui.configuration

import android.os.Bundle
import android.view.View
import androidx.navigation.findNavController
import kotlinx.android.synthetic.main.fragment_select_configuration.*
import pl.tul.advancedmobileapplications.ui.R
import pl.tul.advancedmobileapplications.ui.ui.common.BaseFragment


class SelectConfigurationFragment : BaseFragment(R.layout.fragment_select_configuration) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupListeners()
    }

    private fun setupListeners() {
        showLogsButton.setOnClickListener {
            val action = SelectConfigurationFragmentDirections
                .actionSelectConfigurationFragmentToDisplayLogsFragment()
            view?.findNavController()?.navigate(action)
        }
        getConfigurationButton.setOnClickListener {
            configurationIdInput.text.toString().toIntOrNull()?.let { configurationId ->
                val action = SelectConfigurationFragmentDirections
                    .actionSelectConfigurationFragmentToLocationTrackingFragment(
                        configurationId,
                        true
                    )
                view?.findNavController()?.navigate(action)
            }
        }
    }
}