package pl.tul.advancedmobileapplications.ui.di.component

import android.app.Application
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import pl.tul.advancedmobileapplications.business.di.component.BusinessComponent
import pl.tul.advancedmobileapplications.ui.GeoFenceApplication
import pl.tul.advancedmobileapplications.ui.di.module.ActivityModule
import pl.tul.advancedmobileapplications.ui.di.module.ApplicationBindingModule
import pl.tul.advancedmobileapplications.ui.di.module.FragmentModule
import pl.tul.advancedmobileapplications.ui.di.module.ServiceModule
import pl.tul.advancedmobileapplications.ui.di.scope.UiModuleScope

@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        ApplicationBindingModule::class,
        ActivityModule::class,
        FragmentModule::class,
        ServiceModule::class
    ],
    dependencies = [
        BusinessComponent::class
    ]
)
@UiModuleScope
interface ApplicationComponent : AndroidInjector<GeoFenceApplication> {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): Builder

        fun businessComponent(businessComponent: BusinessComponent): Builder

        fun build(): ApplicationComponent
    }
}