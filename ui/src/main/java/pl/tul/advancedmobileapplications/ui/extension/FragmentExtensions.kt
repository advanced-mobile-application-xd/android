package pl.tul.advancedmobileapplications.ui.extension

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders
import pl.tul.advancedmobileapplications.ui.ui.common.BaseActivity
import pl.tul.advancedmobileapplications.ui.ui.common.BaseFragment

inline fun <reified T : ViewModel> BaseFragment.viewModel(): Lazy<T> = lazy {
    ViewModelProviders.of(this, viewModelFactory).get(T::class.java)
}

fun BaseFragment.askForPermission(
    permission: String,
    onDenied: (permanentlyDenied: Boolean) -> Unit,
    onSuccess: () -> Unit
) {
    val baseActivity = activity?.takeIfValid() as? BaseActivity
    baseActivity?.askForPermission(permission, onDenied, onSuccess)
}

inline fun <reified T> Fragment.lazyArgument(
    key: String,
    defaultValue: T
): Lazy<T> = lazy {
    arguments?.get(key) as? T ?: defaultValue
}

inline fun <reified T> Fragment.lazyArgument(
    key: String
): Lazy<T?> = lazy {
    arguments?.get(key) as? T
}