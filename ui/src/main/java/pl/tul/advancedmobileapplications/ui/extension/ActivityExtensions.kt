package pl.tul.advancedmobileapplications.ui.extension

import android.app.Activity
import android.app.ActivityManager
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.provider.Settings
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders
import pl.tul.advancedmobileapplications.ui.ui.common.BaseActivity

inline fun <reified T : ViewModel> BaseActivity.viewModel(): Lazy<T> = lazy {
    ViewModelProviders.of(this, viewModelFactory).get(T::class.java)
}

fun BaseActivity.askForPermission(
    permission: String,
    onDenied: (permanentlyDenied: Boolean) -> Unit,
    onSuccess: () -> Unit
) {
    permissionManager.askForPermission(this, permission, onDenied, onSuccess)
}

inline fun <reified T : Activity> T.takeIfValid(): T? {
    return if (isValid()) {
        this
    } else {
        null
    }
}

fun Activity.isValid(): Boolean = !isFinishing && !isDestroyed

fun Activity.navigateToSettings() {
    startActivity(Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS).apply {
        data = Uri.fromParts("package", packageName, null)
        addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
        addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS)
    })
}

inline fun <reified T> Activity.lazyExtra(
    key: String,
    defaultValue: T
): Lazy<T> = lazy {
    intent?.extras?.get(key) as? T ?: defaultValue
}

inline fun <reified T> Activity.lazyExtra(
    key: String
): Lazy<T?> = lazy {
    intent?.extras?.get(key) as? T
}

fun Activity.isServiceRunning(serviceClass: Class<*>): Boolean {
    val manager = getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager?
    for (service in manager!!.getRunningServices(Integer.MAX_VALUE)) {
        if (serviceClass.name == service.service.className) {
            return true
        }
    }
    return false
}