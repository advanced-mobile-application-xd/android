package pl.tul.advancedmobileapplications.ui.di.module

import dagger.Module
import dagger.android.ContributesAndroidInjector
import pl.tul.advancedmobileapplications.ui.service.LocationTrackingService

@Module
interface ServiceModule {

    @ContributesAndroidInjector
    fun locationTrackingService(): LocationTrackingService
}