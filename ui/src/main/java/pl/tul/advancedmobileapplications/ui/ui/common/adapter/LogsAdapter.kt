package pl.tul.advancedmobileapplications.ui.ui.common.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_log_entry.view.*
import pl.tul.advancedmobileapplications.model.logger.LogEntry
import pl.tul.advancedmobileapplications.ui.R
import java.text.SimpleDateFormat
import java.util.*

class LogsAdapter : RecyclerView.Adapter<LogsAdapter.LogViewHolder>() {

    private val _logs = mutableListOf<LogEntry>()
    var logs: List<LogEntry>
        get() = _logs.toList()
        set(newLogs) {
            val diffCallback = LogEntryDiffCallback(_logs, newLogs)
            val diffResult = DiffUtil.calculateDiff(diffCallback)
            _logs.clear()
            _logs.addAll(newLogs)
            diffResult.dispatchUpdatesTo(this)
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LogViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.item_log_entry, parent, false)
        return LogViewHolder(view)
    }

    override fun getItemCount(): Int = _logs.size

    override fun onBindViewHolder(holder: LogViewHolder, position: Int) {
        holder.bind(_logs[position])
    }

    class LogViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(log: LogEntry) {
            with(itemView) {
                logDateTime.text = toDateString(log.timestamp)
                logMessage.text = log.message
            }
        }

        private fun toDateString(timestamp: Long): String? {
            return try {
                val sdf = SimpleDateFormat(LOG_DATE_TIME_FORMAT, Locale.getDefault())
                val date = Date(timestamp)
                sdf.format(date)
            } catch (e: Exception) {
                null
            }
        }

        companion object {
            const val LOG_DATE_TIME_FORMAT = "dd.MM.yyyy\nHH:mm"
        }
    }

    private class LogEntryDiffCallback(
        private val oldLogs: List<LogEntry>,
        private val newLogs: List<LogEntry>
    ) : DiffUtil.Callback() {

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
            oldLogs[oldItemPosition].id == newLogs[newItemPosition].id

        override fun getOldListSize(): Int = oldLogs.size

        override fun getNewListSize(): Int = newLogs.size

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
            oldLogs[oldItemPosition] == newLogs[newItemPosition]
    }
}