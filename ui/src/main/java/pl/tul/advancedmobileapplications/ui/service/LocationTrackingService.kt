package pl.tul.advancedmobileapplications.ui.service

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.IBinder
import androidx.core.app.NotificationCompat
import com.google.android.gms.location.ActivityRecognitionResult
import dagger.android.DaggerService
import pl.tul.advancedmobileapplications.business.location.LocationTracker
import pl.tul.advancedmobileapplications.business.logger.SingleConnectionLogger
import pl.tul.advancedmobileapplications.ui.R
import pl.tul.advancedmobileapplications.ui.ui.main.MainActivity
import javax.inject.Inject


class LocationTrackingService : DaggerService() {

    @Inject
    lateinit var logger: SingleConnectionLogger

    @Inject
    lateinit var locationTracker: LocationTracker

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        createNotificationChannel()

        when (intent?.action) {
            ACTION_START_SERVICE -> {
                val configurationId = intent.getIntExtra(INTENT_EXTRA_CONFIGURATION_ID, 0)
                val intervalInMinutes = intent.getIntExtra(
                    INTENT_EXTRA_INTERVAL_IN_MINUTES,
                    DEFAULT_INTERVAL_IN_MINUTES
                )
                val trackedObjectId = intent.getStringExtra(INTENT_EXTRA_TRACKED_OBJECT_ID) ?: ""

                val pendingDetailsIntent = PendingIntent.getActivity(
                    this,
                    0,
                    MainActivity.getTrackingDetailsIntent(this, configurationId),
                    PendingIntent.FLAG_CANCEL_CURRENT
                )

                val pendingStopServiceIntent = PendingIntent.getService(
                    this,
                    0,
                    getStopServiceIntent(this, "Powiadomienie"),
                    PendingIntent.FLAG_CANCEL_CURRENT
                )

                val notification = NotificationCompat.Builder(this, CHANNEL_ID)
                    .setContentTitle(getString(R.string.label_location_tracking_notification_title))
                    .setContentText(getString(R.string.label_location_tracking_notification_content))
                    .setContentIntent(pendingDetailsIntent)
                    .setSmallIcon(R.drawable.ic_launcher_foreground)
                    .addAction(
                        R.drawable.ic_launcher_foreground,
                        getString(R.string.label_stop_tracking),
                        pendingStopServiceIntent
                    )
                    .build()
                startForeground(NOTIFICATION_ID, notification)

                locationTracker.startLogging(
                    trackedObjectId,
                    intervalInMinutes,
                    getActivityRecognitionPendingIntent(this)
                )
            }
            ACTION_STOP_SERVICE -> {
                logger.log(
                    getString(
                        R.string.log_location_tracking_stop_request,
                        intent.getStringExtra(INTENT_EXTRA_EVENT_SOURCE)
                    )
                )
                stopForeground(true)
                stopSelf()
            }
        }

        if (ActivityRecognitionResult.hasResult(intent)) {
            val result = ActivityRecognitionResult.extractResult(intent)
            locationTracker.updateDetectedActivity(result.mostProbableActivity)
        }

        return Service.START_NOT_STICKY
    }

    override fun onDestroy() {
        locationTracker.stopLogging()
        super.onDestroy()
    }

    private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val serviceChannel = NotificationChannel(
                CHANNEL_ID,
                getString(R.string.channel_name_location_tracking_service),
                NotificationManager.IMPORTANCE_MIN
            )
            val manager = getSystemService(NotificationManager::class.java)
            manager!!.createNotificationChannel(serviceChannel)
        }
    }

    companion object {

        const val CHANNEL_ID = "LocationTrackingServiceChannel"

        const val DEFAULT_INTERVAL_IN_MINUTES = 3

        private const val NOTIFICATION_ID = 1
        private const val ACTION_STOP_SERVICE = "ACTION_STOP_SERVICE"

        private const val ACTION_START_SERVICE = "ACTION_START_SERVICE"
        private const val INTENT_EXTRA_CONFIGURATION_ID = "EXTRA_CONFIGURATION_ID"
        private const val INTENT_EXTRA_TRACKED_OBJECT_ID = "EXTRA_TRACKED_OBJECT_ID"
        private const val INTENT_EXTRA_INTERVAL_IN_MINUTES = "EXTRA_INTERVAL_IN_MINUTES"

        private const val INTENT_EXTRA_EVENT_SOURCE = "EXTRA_EVENT_SOURCE"

        fun getStopServiceIntent(context: Context, eventSource: String): Intent {
            val intent = Intent(context, LocationTrackingService::class.java)
            intent.action = ACTION_STOP_SERVICE
            intent.putExtra(INTENT_EXTRA_EVENT_SOURCE, eventSource)
            return intent
        }

        fun getStartServiceIntent(
            context: Context,
            configurationId: Int,
            trackedObjectId: String,
            intervalInMinutes: Int
        ): Intent {
            val intent = Intent(context, LocationTrackingService::class.java)
            intent.action = ACTION_START_SERVICE
            intent.putExtra(INTENT_EXTRA_CONFIGURATION_ID, configurationId)
            intent.putExtra(INTENT_EXTRA_TRACKED_OBJECT_ID, trackedObjectId)
            intent.putExtra(INTENT_EXTRA_INTERVAL_IN_MINUTES, intervalInMinutes)
            return intent
        }

        private fun getActivityRecognitionPendingIntent(context: Context): PendingIntent =
            PendingIntent.getService(
                context,
                0,
                Intent(context, LocationTrackingService::class.java),
                PendingIntent.FLAG_UPDATE_CURRENT
            )
    }
}