package pl.tul.advancedmobileapplications.ui.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.StringRes
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.Group
import kotlinx.android.synthetic.main.mask_error.view.*
import kotlinx.android.synthetic.main.mask_loading.view.*
import pl.tul.advancedmobileapplications.ui.R

class ConstraintLayoutWithMask @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    private val loadingOverlay by lazy {
        LayoutInflater.from(context).inflate(R.layout.mask_loading, this, true)
    }
    private val errorOverlay by lazy {
        LayoutInflater.from(context).inflate(R.layout.mask_error, this, true)
    }
    private val contentGroup by lazy {
        Group(context)
    }
    private val contentGroupReferencedIds =
        mutableListOf<Int>() // WARNING: Removing views not implemented!
    private val views = mutableListOf<View>()

    private var groupsSet = false

    init {
        if (!isInEditMode) {
            setupGroups()
            applyInitialOverlay(context, attrs)
        }
    }

    override fun addView(child: View?) {
        addViewToContent(child)
        super.addView(child)
    }

    override fun addView(child: View?, index: Int) {
        addViewToContent(child)
        super.addView(child, index)
    }

    override fun addView(child: View?, width: Int, height: Int) {
        addViewToContent(child)
        super.addView(child, width, height)
    }

    override fun addView(child: View?, params: ViewGroup.LayoutParams?) {
        addViewToContent(child)
        super.addView(child, params)
    }

    override fun addView(child: View?, index: Int, params: ViewGroup.LayoutParams?) {
        addViewToContent(child)
        super.addView(child, index, params)
    }

    fun showContent(hiddenVisibility: Int = View.GONE): Unit =
        showOnly(contentGroup, hiddenVisibility)

    fun showLoadingOverlay(hiddenVisibility: Int = View.GONE): Unit =
        showOnly(loadingOverlay.loadingMaskGroup, hiddenVisibility)

    fun showMaskOverlay(maskData: MaskData, hiddenVisibility: Int = View.GONE) {
        with(errorOverlay) {
            maskData.title?.let {
                titleText.text = it
                titleText.visibility = View.VISIBLE
            } ?: run {
                titleText.visibility = View.GONE
            }
            maskData.description?.let {
                descriptionText.text = it
                descriptionText.visibility = View.VISIBLE
            } ?: run {
                descriptionText.visibility = View.GONE
            }
            maskData.retryButtonText?.let { text ->
                retryButton.text = text
                retryButton.setOnClickListener {
                    maskData.retryButtonCallback()
                }
                retryButton.visibility = View.VISIBLE
            } ?: run {
                retryButton.visibility = View.GONE
            }
        }
        showOnly(errorOverlay.errorMaskGroup, hiddenVisibility)
    }

    fun showNothing(hiddenVisibility: Int = View.GONE) {
        views.forEach { view ->
            view.visibility = hiddenVisibility
        }
    }

    private fun setupGroups() {
        views.apply {
            add(loadingOverlay.loadingMaskGroup)
            add(errorOverlay.errorMaskGroup)
            add(contentGroup)
        }
        addView(contentGroup)
        groupsSet = true
    }

    private fun addViewToContent(child: View?) {
        if (child != null && groupsSet) {
            contentGroupReferencedIds.add(child.id)
            contentGroup.referencedIds = contentGroupReferencedIds.toIntArray()
        }
    }

    private fun applyInitialOverlay(context: Context, attrs: AttributeSet?) {
        attrs?.let { attributeSet ->
            val styledAttributes = context.obtainStyledAttributes(
                attributeSet,
                R.styleable.ConstraintLayoutWithMask,
                0,
                0
            )
            val initialOverlay = styledAttributes.getInteger(
                R.styleable.ConstraintLayoutWithMask_initialOverlay,
                INITIAL_OVERLAY_CONTENT
            )
            when (initialOverlay) {
                INITIAL_OVERLAY_LOADING -> showLoadingOverlay()
                INITIAL_OVERLAY_NOTHING -> showNothing()
                else -> showContent()
            }
            styledAttributes.recycle()
        }
    }

    private fun showOnly(viewToShow: View, hiddenVisibility: Int = View.GONE) {
        views.filterNot {
            it == viewToShow
        }.forEach {
            it.visibility = hiddenVisibility
        }
        viewToShow.visibility = View.VISIBLE
    }

    data class MaskData(
        val title: String? = null,
        val description: String? = null,
        val retryButtonText: String? = null,
        val retryButtonCallback: () -> Unit = {}
    ) {
        class Builder(private val context: Context) {

            private var title: String? = null
            private var description: String? = null
            private var retryButtonText: String? = null
            private var retryButtonCallback: () -> Unit = {}

            fun title(title: String) = apply {
                this.title = title
            }

            fun title(@StringRes title: Int) =
                title(context.getString(title))

            fun description(description: String) = apply {
                this.description = description
            }

            fun description(@StringRes description: Int) =
                description(context.getString(description))

            fun retryButton(label: String, action: () -> Unit) = apply {
                this.retryButtonText = label
                this.retryButtonCallback = action
            }

            fun retryButton(@StringRes label: Int, action: () -> Unit) =
                retryButton(context.getString(label), action)

            fun build() = MaskData(
                title,
                description,
                retryButtonText,
                retryButtonCallback
            )
        }
    }

    companion object {
        const val INITIAL_OVERLAY_NOTHING = 0
        const val INITIAL_OVERLAY_LOADING = 1
        const val INITIAL_OVERLAY_CONTENT = 2
    }
}