package pl.tul.advancedmobileapplications.ui

import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication
import pl.tul.advancedmobileapplications.business.di.component.BusinessComponent
import pl.tul.advancedmobileapplications.business.di.component.DaggerBusinessComponent
import pl.tul.advancedmobileapplications.business.di.provider.BusinessComponentProvider
import pl.tul.advancedmobileapplications.ui.di.component.DaggerApplicationComponent

class GeoFenceApplication : DaggerApplication(), BusinessComponentProvider {

    private val businessComponent by lazy {
        DaggerBusinessComponent.builder()
            .context(this)
            .build()
    }

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> =
        DaggerApplicationComponent.builder()
            .application(this)
            .businessComponent(provideBusinessComponent())
            .build()

    override fun provideBusinessComponent(): BusinessComponent = businessComponent
}
