package pl.tul.advancedmobileapplications.ui.ui.common

import androidx.lifecycle.ViewModelProvider
import dagger.android.support.DaggerAppCompatActivity
import pl.tul.advancedmobileapplications.business.permission.PermissionManager
import javax.inject.Inject

abstract class BaseActivity : DaggerAppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var permissionManager: PermissionManager
}