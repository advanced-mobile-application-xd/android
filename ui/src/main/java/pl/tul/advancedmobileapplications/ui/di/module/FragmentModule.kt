package pl.tul.advancedmobileapplications.ui.di.module

import dagger.Module
import dagger.android.ContributesAndroidInjector
import pl.tul.advancedmobileapplications.ui.ui.configuration.SelectConfigurationFragment
import pl.tul.advancedmobileapplications.ui.ui.logs.DisplayLogsFragment
import pl.tul.advancedmobileapplications.ui.ui.tracking.LocationTrackingFragment

@Module
interface FragmentModule {

    @ContributesAndroidInjector
    fun selectConfigurationFragment(): SelectConfigurationFragment

    @ContributesAndroidInjector
    fun displayLogsFragment(): DisplayLogsFragment

    @ContributesAndroidInjector
    fun locationTrackingFragment(): LocationTrackingFragment
}