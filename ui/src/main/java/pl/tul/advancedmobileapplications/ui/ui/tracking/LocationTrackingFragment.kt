package pl.tul.advancedmobileapplications.ui.ui.tracking

import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.fragment_location_tracking.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.noButton
import org.jetbrains.anko.yesButton
import pl.tul.advancedmobileapplications.business.viewmodel.LocationTrackingViewModel
import pl.tul.advancedmobileapplications.model.api.ConfigurationResponse
import pl.tul.advancedmobileapplications.model.logger.LogEntry
import pl.tul.advancedmobileapplications.model.resource.Resource
import pl.tul.advancedmobileapplications.model.resource.Status
import pl.tul.advancedmobileapplications.ui.R
import pl.tul.advancedmobileapplications.ui.extension.askForPermission
import pl.tul.advancedmobileapplications.ui.extension.navigateToSettings
import pl.tul.advancedmobileapplications.ui.extension.takeIfValid
import pl.tul.advancedmobileapplications.ui.extension.viewModel
import pl.tul.advancedmobileapplications.ui.service.LocationTrackingService
import pl.tul.advancedmobileapplications.ui.ui.common.BaseFragment
import pl.tul.advancedmobileapplications.ui.ui.common.adapter.LogsAdapter
import pl.tul.advancedmobileapplications.ui.view.ConstraintLayoutWithMask

class LocationTrackingFragment : BaseFragment(R.layout.fragment_location_tracking) {

    private val viewModel: LocationTrackingViewModel by viewModel()
    private val args: LocationTrackingFragmentArgs by navArgs()
    private val logsAdapter by lazy { LogsAdapter() }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupLogsRecycler()
        setupListeners()
        setupObservers()
        loadConfiguration()
    }

    private fun setupLogsRecycler() {
        logsRecycler.apply {
            layoutManager =
                LinearLayoutManager(
                    this@LocationTrackingFragment.context,
                    RecyclerView.VERTICAL,
                    false
                )
            adapter = logsAdapter
            addItemDecoration(
                DividerItemDecoration(
                    this@LocationTrackingFragment.context,
                    RecyclerView.VERTICAL
                )
            )
        }
    }

    private fun setupListeners() {
        stopServiceButton.setOnClickListener {
            activity?.takeIfValid()?.alert(
                R.string.alert_stop_service_message,
                R.string.alert_stop_service_title
            ) {
                yesButton {
                    activity?.takeIfValid()?.let { activity ->
                        val serviceIntent =
                            LocationTrackingService.getStopServiceIntent(
                                activity,
                                "Zatrzymanie śledzenia"
                            )
                        activity.stopService(serviceIntent)
                        view?.findNavController()?.navigateUp()
                    }
                }
                noButton {
                    // Do nothing
                }
            }?.show()
        }
        clearLogsButton.setOnClickListener {
            activity?.takeIfValid()?.alert(
                R.string.alert_clear_logs_message,
                R.string.alert_clear_logs_title
            ) {
                yesButton {
                    viewModel.clearLogs()
                }
                noButton {
                    // Do nothing
                }
            }?.show()
        }
    }

    private fun setupObservers() {
        viewModel.logs.observe(viewLifecycleOwner, Observer {
            updateLogs(it ?: listOf())
        })
        viewModel.configuration.observe(viewLifecycleOwner, Observer {
            it?.let { configuration ->
                onConfigurationResponse(configuration)
            }
        })
    }

    private fun loadConfiguration() {
        if (!args.startNew || viewModel.configuration.value?.status == Status.SUCCESS) {
            onConfigurationLoadedSuccessfully()
        } else {
            viewModel.getConfiguration(args.configurationId)
        }
    }

    private fun updateLogs(logs: List<LogEntry>) {
        logsAdapter.logs = logs
    }

    private fun onConfigurationResponse(configuration: Resource<ConfigurationResponse>) {
        when (configuration.status) {
            Status.LOADING -> root.showLoadingOverlay()
            Status.SUCCESS -> onConfigurationLoadedSuccessfully()
            Status.ERROR -> {
                val errorMessage = configuration.errorMessage
                    ?: getString(R.string.label_unknown_error)
                val mask = ConstraintLayoutWithMask.MaskData.Builder(requireContext())
                    .title(getString(R.string.label_an_error_occured))
                    .description(errorMessage)
                    .retryButton(R.string.label_try_again) {
                        loadConfiguration()
                    }
                    .build()
                root.showMaskOverlay(mask)
            }
            Status.NO_CONNECTION -> {
                val mask = ConstraintLayoutWithMask.MaskData.Builder(requireContext())
                    .title(R.string.label_no_internet_connection)
                    .description(R.string.label_no_internet_connection_description)
                    .retryButton(R.string.label_try_again) {
                        loadConfiguration()
                    }
                    .build()
                root.showMaskOverlay(mask)
            }
        }
    }

    private fun onConfigurationLoadedSuccessfully() {

        // FIXME: Powrót z ustawień z nadanymi uprawnieniami nie odświeża widoku
        askForPermission(
            permission = android.Manifest.permission.ACCESS_FINE_LOCATION,
            onDenied = { permanentlyDenied ->
                val retryButtonLabel = if (permanentlyDenied) {
                    R.string.label_go_to_settings
                } else {
                    R.string.label_try_again
                }
                val mask = ConstraintLayoutWithMask.MaskData.Builder(requireContext())
                    .title(R.string.label_missing_permission)
                    .description(R.string.label_missing_permission_content)
                    .retryButton(retryButtonLabel) {
                        if (permanentlyDenied) {
                            activity?.takeIfValid()?.navigateToSettings()
                        } else {
                            onConfigurationLoadedSuccessfully()
                        }
                    }
                    .build()
                root.showMaskOverlay(mask)
            },
            onSuccess = {
                if (args.startNew) {
                    activity?.takeIfValid()?.let { activity ->
                        val serviceIntent =
                            LocationTrackingService.getStartServiceIntent(
                                activity,
                                args.configurationId,
                                viewModel.configuration.value?.data?.trackedObjectId ?: "",
                                viewModel.configuration.value?.data?.positionIntervalInMinutes
                                    ?: LocationTrackingService.DEFAULT_INTERVAL_IN_MINUTES
                            )
                        ContextCompat.startForegroundService(activity, serviceIntent)
                    }
                }
                root.showContent()
            }
        )
    }
}