package pl.tul.advancedmobileapplications.ui.di.scope

import javax.inject.Scope

@Scope
@Retention
annotation class UiModuleScope