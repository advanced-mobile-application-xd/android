package pl.tul.advancedmobileapplications.ui.di.module

import dagger.Module
import dagger.android.ContributesAndroidInjector
import pl.tul.advancedmobileapplications.ui.ui.main.MainActivity

@Module
interface ActivityModule {

    @ContributesAndroidInjector
    fun mainActivity(): MainActivity
}