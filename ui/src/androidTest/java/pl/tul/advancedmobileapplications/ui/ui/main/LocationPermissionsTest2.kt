package pl.tul.advancedmobileapplications.ui.ui.main


import androidx.test.espresso.DataInteraction
import androidx.test.espresso.ViewInteraction
import androidx.test.filters.LargeTest
import androidx.test.rule.ActivityTestRule
import androidx.test.rule.GrantPermissionRule
import androidx.test.runner.AndroidJUnit4
import android.view.View
import android.view.ViewGroup
import android.view.ViewParent

import androidx.test.InstrumentationRegistry.getInstrumentation
import androidx.test.espresso.Espresso.onData
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.Espresso.pressBack
import androidx.test.espresso.Espresso.openActionBarOverflowOrOptionsMenu
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.*
import androidx.test.espresso.matcher.ViewMatchers.*

import pl.tul.advancedmobileapplications.ui.R

import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.TypeSafeMatcher
import org.hamcrest.core.IsInstanceOf
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

import org.hamcrest.Matchers.allOf
import org.hamcrest.Matchers.anything
import org.hamcrest.Matchers.`is`

@LargeTest
@RunWith(AndroidJUnit4::class)
class LocationPermissionsTest2 {

    @Rule
    @JvmField
    var mActivityTestRule = ActivityTestRule(MainActivity::class.java)

    @Rule
    @JvmField
    var mGrantPermissionRule =
            GrantPermissionRule.grant(
                    "android.permission.ACCESS_FINE_LOCATION")

    @Test
    fun locationPermissionsTest2() {
        val textInputEditText = onView(
allOf(withId(R.id.configurationIdInput),
childAtPosition(
childAtPosition(
withId(R.id.textInputLayout),
0),
0),
isDisplayed()))
        textInputEditText.perform(replaceText("5"), closeSoftKeyboard())
        
        val appCompatButton = onView(
allOf(withId(R.id.getConfigurationButton), withText("Pobierz konfigurację"),
childAtPosition(
childAtPosition(
withId(R.id.navHostFragment),
0),
1),
isDisplayed()))
        appCompatButton.perform(click())
        
        val appCompatButton2 = onView(
allOf(withId(R.id.retryButton), withText("Spróbuj ponownie"),
childAtPosition(
allOf(withId(R.id.root),
childAtPosition(
withId(R.id.navHostFragment),
0)),
5),
isDisplayed()))
        appCompatButton2.perform(click())
        
        val button = onView(
allOf(withId(R.id.retryButton),
childAtPosition(
allOf(withId(R.id.root),
childAtPosition(
withId(R.id.navHostFragment),
0)),
3),
isDisplayed()))
        button.check(matches(isDisplayed()))
        }
    
    private fun childAtPosition(
            parentMatcher: Matcher<View>, position: Int): Matcher<View> {

        return object : TypeSafeMatcher<View>() {
            override fun describeTo(description: Description) {
                description.appendText("Child at position $position in parent ")
                parentMatcher.describeTo(description)
            }

            public override fun matchesSafely(view: View): Boolean {
                val parent = view.parent
                return parent is ViewGroup && parentMatcher.matches(parent)
                        && view == parent.getChildAt(position)
            }
        }
    }
    }
