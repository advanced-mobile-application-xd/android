package pl.tul.advancedmobileapplications.ui.ui.main


import android.view.View
import android.view.ViewGroup
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.filters.LargeTest
import androidx.test.rule.ActivityTestRule
import androidx.test.rule.GrantPermissionRule
import androidx.test.runner.AndroidJUnit4
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.Matchers.allOf
import org.hamcrest.TypeSafeMatcher
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import pl.tul.advancedmobileapplications.ui.R
import java.net.HttpURLConnection

@LargeTest
@RunWith(AndroidJUnit4::class)
class LocationPermissionsTest {

    @Rule
    @JvmField
    var mActivityTestRule = ActivityTestRule(MainActivity::class.java)

    @Rule
    @JvmField
    var mGrantPermissionRule =
        GrantPermissionRule.grant(
            "android.permission.ACCESS_FINE_LOCATION"
        )

    lateinit var mockWebServer: MockWebServer
    @Before
    fun setUp() {
        mockWebServer = MockWebServer()
        val mockResponse = MockResponse()
                .setResponseCode(HttpURLConnection.HTTP_OK)
        mockWebServer.enqueue(mockResponse)
        mockWebServer.start()
    }

    @Test
    fun locationPermissionsDeniedTest() {
        val textInputEditText = onView(
            allOf(
                withId(R.id.configurationIdInput),
                isDisplayed()
            )
        )
        textInputEditText.perform(replaceText("5"), closeSoftKeyboard())

        val appCompatButton = onView(
            allOf(
                withId(R.id.getConfigurationButton),
                isDisplayed()
            )
        )
        appCompatButton.perform(click())

        val button = onView(
            allOf(
                withId(R.id.retryButton),
                isDisplayed()
            )
        )
        button.check(matches(isDisplayed()))
        button.check(matches(withText("Spróbuj ponownie")))

        val textView = onView(
            allOf(
                withId(R.id.titleText),
                isDisplayed()
            )
        )
        textView.check(matches(withText("Brak wymaganych uprawnień")))

        val textView2 = onView(
            allOf(
                withId(R.id.descriptionText),
                isDisplayed()
            )
        )
        textView2.check(matches(withText("Aby korzystać z funkcji śledzenia lokalizacji wymagane jest przyznanie odpowiednich uprawnień systemowych")))
    }


    @Test
    fun locationPermissionsPemanentlyDeniedTest() {
        val textInputEditText = onView(
            allOf(
                withId(R.id.configurationIdInput),
                childAtPosition(
                    childAtPosition(
                        withId(R.id.textInputLayout),
                        0
                    ),
                    0
                ),
                isDisplayed()
            )
        )
        textInputEditText.perform(replaceText("5"), closeSoftKeyboard())

        val appCompatButton = onView(
            allOf(
                withId(R.id.getConfigurationButton), withText("Pobierz konfigurację"),
                childAtPosition(
                    childAtPosition(
                        withId(R.id.navHostFragment),
                        0
                    ),
                    1
                ),
                isDisplayed()
            )
        )
        appCompatButton.perform(click())

        val appCompatButton2 = onView(
            allOf(
                withId(R.id.retryButton), withText("Spróbuj ponownie"),
                childAtPosition(
                    allOf(
                        withId(R.id.root),
                        childAtPosition(
                            withId(R.id.navHostFragment),
                            0
                        )
                    ),
                    5
                ),
                isDisplayed()
            )
        )
        appCompatButton2.perform(click())

        val button = onView(
            allOf(
                withId(R.id.retryButton),
                isDisplayed()
            )
        )
        button.check(matches(isDisplayed()))
        button.check(matches(withText("Przejdź do ustawień")))
    }

    private fun childAtPosition(
        parentMatcher: Matcher<View>, position: Int
    ): Matcher<View> {

        return object : TypeSafeMatcher<View>() {
            override fun describeTo(description: Description) {
                description.appendText("Child at position $position in parent ")
                parentMatcher.describeTo(description)
            }

            public override fun matchesSafely(view: View): Boolean {
                val parent = view.parent
                return parent is ViewGroup && parentMatcher.matches(parent)
                        && view == parent.getChildAt(position)
            }
        }
    }
}
